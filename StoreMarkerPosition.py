﻿#C:\Users\PerForM Labs\Kamran's Workspace\Kamran-Codes\Recording Marker Position
# MyTestCode.py
# This Code records the position of the Markers into a .txt file

import sys
import time
import numpy
from OWL import *
import array
import datetime
import viz

# change these to match your configuration



MARKER_COUNT = 50;
SERVER_NAME = "192.168.1.230";
INIT_FLAGS = 0;
TimeInterval = 0.00208
class MarkerClass:
    def __init__(self, X, Y, Z, Cond):
        self.X = [];
        self.Y = [];
        self.Z = [];
        self.Cond = [];
        self.frame= [];
    #fed
#ssalc

class RigidBodyClass:
    def __init__(self, Marker):
        self.Marker[0:MARKER_COUNT] = [];
    #fed
#ssalc
global StoreDataFlag
StoreDataFlag = False
ExitFlag = False;
TotalFrameCount = 1000
FrameCount = 0;

def OnKeyDown(key):
	global StoreDataFlag
	global FrameCount
	if (key == 'r'):
		# start streaming
		owlSetInteger(OWL_STREAMING, OWL_ENABLE)
		print 'Storing Data Started ==>\n'
		StoreDataFlag = True
		FrameCount = 0;
	if (key == 'e'):
		ExitFlag = True
		# cleanup
		owlDone();
		OutputTextFile.flush()
		OutputTextFile.close()
		print 'Text File Generated & Closed ==> Server Down'
def MyTimer(num):
	global StoreDataFlag
	global FrameCount
	global markers
	global OutputString
	if(StoreDataFlag == True):
		markers = owlGetMarkers()
		# check for error	
		err = owlGetError()
		if(err != OWL_NO_ERROR):
			print 'Server Error'
			return

		if ( len(markers) > 0 ):
			if( len(markers) > 0 and markers[17].cond > 0 ) :
				#print "number of Markers are : %d" % len(markers);
				OutputString  =  ' FrameTime %f ' %(viz.getFrameTime())
				OutputString = OutputString + ' FrameNumber %d '%(FrameCount) 
				OutputString = 	OutputString + ' MarkerPos_XYZ '
				for i in range(17, 25):
					OutputString = OutputString + ' %f %f %f' %(markers[i].x, markers[i].y, markers[i].z)
					#print "Marker %s : {COND: %2.1f} {FRAME: %d} {POS: [%.2f, %.2f, %.2f]} " % ( str(i), markers[i].cond, markers[i].frame, markers[i].x, markers[i].y, markers[i].z);
				#print 'FrameCount =', FrameCount, '\n'
				OutputTextFile.write(OutputString + '\n');
			else:
				for i in range(17, 25):
					print 'Marker.cond[',i,']=', markers[i].cond
		
		FrameCount = FrameCount + 1
		if ( FrameCount > TotalFrameCount):
			StoreDataFlag = False;
			owlSetInteger(OWL_STREAMING, OWL_DISABLE)
			print FrameCount, 'Frames Recorded'


def owl_print_error(s, n):
    """Print OWL error."""
    if(n < 0): print "%s: %d" % (s, n)
    elif(n == OWL_NO_ERROR): print "%s: No Error" % s
    elif(n == OWL_INVALID_VALUE): print "%s: Invalid Value" % s
    elif(n == OWL_INVALID_ENUM): print "%s: Invalid Enum" % s
    elif(n == OWL_INVALID_OPERATION): print "%s: Invalid Operation" % s
    else: print "%s: 0x%x" % (s, n)

viz.callback(viz.KEYDOWN_EVENT, OnKeyDown)
if(owlInit(SERVER_NAME, INIT_FLAGS) < 0):
    print "init error: ", owlGetError()
    sys.exit(0)
else :
    print ">>> Connection Established <<<"
# create tracker 0
tracker = 0
owlTrackeri(tracker, OWL_CREATE, OWL_POINT_TRACKER)

# set markers
for i in range(MARKER_COUNT):
    owlMarkeri(MARKER(tracker, i), OWL_SET_LED, i)

# activate tracker
owlTracker(tracker, OWL_ENABLE)

# flush requests and check for errors
if(owlGetStatus() == 0):
    owl_print_error("error in point tracker setup", owlGetError())
    sys.exit(0)

# set define frequency
owlSetFloat(OWL_FREQUENCY, OWL_MAX_FREQUENCY)


#RigidBody = RigidBodyStruct();
MarkersStruct = [];
#MarkersStruct.append(MarkerClass(0, 0, 0, -1));
for i in range(MARKER_COUNT):
    MarkersStruct.append( MarkerClass(0, 0, 0, -1) );
# main loop
#while(1):
global markers
global OutputString
OutputString = '\n'

markers = [];
#for loop in range(NUMBER_OF_FRAMES):
now  = datetime.datetime.now()
dateTimeStr = str(now.year) + '-' + str(now.month) + '-' + str(now.day) + '-' + str(now.hour) + '-' + str(now.minute)
OutputTextFile = open('MarkerPos' + dateTimeStr + '.txt', 'w+')
OutputTextFile.write("#==================================================================================================#");
OutputTextFile.write("\n#======================== Author: Kamran Binaee kamranbinaee@mail.rit.edu =========================#");
OutputTextFile.write("\n#==");
OutputTextFile.write("\n#======================== PerForM Lab && Center For Imaging Science && RIT ========================#");
OutputTextFile.write("\n#==");
OutputTextFile.write("\n#======================== This File is Generated to record the position of the Markers ============#");
OutputTextFile.write("\n#==");
OutputTextFile.write("\n#======================== \KB/ Position Data for Marker ID Number  ==============================#");
OutputTextFile.write("\n#==================================================================================================#\n");
print 'TextFile Opened\n'

viz.go()
viz.setMultiSample(4)
piazza = viz.addChild('piazza.osgb')
viz.callback(viz.TIMER_EVENT,MyTimer)
viz.starttimer(1, TimeInterval, viz.FOREVER)


